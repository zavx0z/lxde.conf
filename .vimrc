"=====================================================
" Vundle settings
"=====================================================
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'		" let Vundle manage Vundle, required

"---------=== Code/project navigation ===-------------
Plugin 'scrooloose/nerdtree' 	    	" Файловый менеджер
Plugin 'vim-scripts/tComment'    	" Комментирование кода
Plugin 'majutsushi/tagbar'              " Class/module менеджер
"------------------=== Other ===----------------------
Plugin 'vim-scripts/vim-auto-save'      " Автосохранение
Plugin 'tpope/vim-obsession'            " Восстановление сессии
Plugin 'bling/vim-airline'   	    	" Статусная строка
Plugin 'vim-airline/vim-airline-themes' " Тема статусной строки

Plugin 'fisadev/FixedTaskList.vim'  	" Pending tasks list
Plugin 'tpope/vim-surround'	   	" Parentheses, brackets, quotes, XML tags, and more
"--------------=== Snippets support ===---------------
Plugin 'garbas/vim-snipmate'		" Snippets manager
Plugin 'MarcWeber/vim-addon-mw-utils'	" dependencies #1
Plugin 'tomtom/tlib_vim'		" dependencies #2
Plugin 'honza/vim-snippets'		" snippets repo
"---------------=== Languages support ===-------------
Plugin 'davidhalter/jedi-vim' 		" Автодополнение 
Plugin 'ervandew/supertab'
" --- Python ---
Plugin 'klen/python-mode'	        " Python mode (docs, refactor, lints, highlighting, run and ipdb and more)
" ---  HTML  ---
Plugin 'vim-scripts/closetag.vim'
Plugin 'othree/xml.vim'

call vundle#end()            		" required

""=====================================================
"" General settings
""=====================================================
" включение распознавания типов файлов и типо-специфичные плагины
filetype on
filetype plugin on

filetype plugin indent on

" отключение режима совместимости с VI
set nocompatible
" отключение пищалки
set t_vb= 
" отключение мигания
set novisualbell       

set shell=/bin/sh

" поддержка virtualenv Pymode
let g:pymode_virtualenv = 1

" отключаем бэкапы и своп-файлы
set nobackup 	     " no backup files
set nowritebackup    " only in case you don't want a backup file while editing
set noswapfile 	     " no swap files

" utf-8 по дефолту в файлах
set fileencoding=utf-8
set encoding=utf-8
set termencoding=utf-8

" leader key
let mapleader = ","

"Удобное поведение backspace
set backspace=indent,eol,start whichwrap+=<,>,[,]

" Русификация горячих клавиш
set langmap=ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;
			\`qwertyuiop[]asdfghjkl\\;'zxcvbnm\\,.~QWERTYUIOP{}ASDFGHJKL:\\"ZXCVBNM<>
nmap Ж :
" yank
nmap Н Y
nmap з p
nmap ф a
nmap щ o
nmap г u
nmap З P

" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %

" подтверждение закрытия не сохраненного файла
set confirm
" автосохранение 
let g:auto_save = 1
" отключить оповещение автосохранения
let g:auto_save_silent = 1
" сохранять во время остановки курсора на 5 сек
let g:auto_save_no_updatetime = 1  
" не удалать пробелы после сохранения
let g:pymode_trim_whitespaces = 0
"=====================================================
" ПОИСК
"=====================================================
set incsearch	     " инкреминтируемый поиск
set hlsearch	     " подсветка результатов поиска

" поиск переменных
" по звездочке не прыгать на следующее найденное, а просто подсветить
nnoremap * *N
" выключить подсветку Ctrl-F8
nnoremap <C-h> :nohlsearch<CR>

"=====================================================
" ТАБЛАЙН
"=====================================================
" включить расширение Airline
let g:airline#extensions#tabline#enabled = 1
" номер таба
let g:airline#extensions#tabline#tab_nr_type = 1 
let g:airline#extensions#tabline#show_tab_nr = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#excludes = ['nerd_tree'] 
let g:airline#extensions#tabline#exclude_preview = 1
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#show_splits = 0
let airline#extensions#tabline#ignore_bufadd_pat =
    \ '\c\vgundo|undotree|vimfiler|tagbar|nerd_tree'
">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> БУФЕРЫ
">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ТАБЫ
" открытие нового таба
nnoremap <C-w>` :tabnew<CR>
" выбор таба
nnoremap <C-w>1 1gt
nnoremap <C-w>2 2gt
nnoremap <C-w>3 3gt
nnoremap <C-w>4 4gt
nnoremap <C-w>5 5gt
nnoremap <C-w>6 6gt
nnoremap <C-w>7 7gt
nnoremap <C-w>8 8gt
nnoremap <C-w>9 9gt
"=====================================================
" ФАЙЛОВЫЙ МЕНЕДЖЕР
"=====================================================
" открыть на F3
map <F3> :NERDTreeToggle<CR>
imap <F3> <esc>:NERDTreeToggle<CR>

" игноррируемые файлы с расширениями
let NERDTreeIgnore=['\~$', '\.pyc$', '\.pyo$', '\.class$', 'pip-log\.txt$', '\.o$']  

" автоматическое отображение при запуске vim
" autocmd vimenter * NERDTree
" autocmd vimenter * if !argc() | NERDTree | endif
"====================================================
" РЕДАКТОР
"=====================================================
" показывать номера строк
set nu	             

" нумерация строк Ctrl+n
function! ToggleNum()
if exists("g:num")
	set number
	unlet g:num
else
	set nonumber
	let g:num = 1
endif
endfunction
nmap <silent> <C-n> :call ToggleNum()<CR>
set number

"не переносить строки
:set wrap

" Режим Paste или NoPaste
function! PasteMode()
	if exists("g:paste")
		set nopaste
		unlet g:paste
	else
		set paste
		let g:paste = 1
	endif
endfunction
nmap <silent> <C-p> :call PasteMode()<CR>

" при переходе за границу в 80 символов в /Python/js/ подсвечиваем на темном фоне текст
augroup vimrc_autocmds
    autocmd!
    autocmd FileType python,javascript highlight Excess ctermbg=Black guibg=Black
    autocmd FileType python,javascript match Excess /\%80v.*/
    autocmd FileType python,javascript set nowrap
augroup END

" спрятать курсор мыши при наборе текста
set mousehide
"=====================================================
" TagBar
"=====================================================
" открыть на F4
map <F4> :TagbarToggle<CR>

" автофокус на Tagbar при открытии
let g:tagbar_autofocus = 0 
" автоматическое отображение при запуске vim
" autocmd vimenter * TagbarToggle
"=====================================================
" СТАТУСНАЯ СТРОКА
"=====================================================
set laststatus=2
let g:airline_theme='base16_ashes'
let g:airline_powerline_fonts = 1
let g:airline_enable_syntastic=1
let g:airline_enable_bufferline=1
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_linecolumn_prefix = '¶ '
let g:airline_paste_symbol = 'ρ'
let g:airline_section_c = '%t'
"=====================================================
"                         КОД
"=====================================================
">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ЗАПУСК
" разрешить запуск кода
let g:pymode_run = 1
let g:pymode_run_bind = '<leader>R'
">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ФОЛДИНГ
" отключить autofold по коду
let g:pymode_folding = 0
"Колоночка, чтобы показывать плюсики для скрытия блоков кода:
" set foldcolumn=1
">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> СИНТАКСИС

" --- Python ---
" переключение на синтаксис
nnoremap <leader>Tp :set ft=python<CR>

" --- HTML, Templates ---
autocmd bufnewfile,bufread *.rhtml setlocal ft=eruby
autocmd BufNewFile,BufRead *.mako setlocal ft=mako
autocmd BufNewFile,BufRead *.tmpl setlocal ft=htmljinja
autocmd BufNewFile,BufRead *.py_tmpl setlocal ft=python
let html_no_rendering=1
let g:closetag_default_xml=1
let g:sparkupNextMapping='<c-l>'
autocmd FileType html,htmldjango,htmljinja,eruby,mako let b:closetag_html_style=1
autocmd FileType html,xhtml,xml,htmldjango,htmljinja,eruby,mako source ~/.vim/scripts/closetag.vim
" переключение на синтаксис
nnoremap <leader>Td :set ft=django<CR>
nnoremap <leader>Th :set ft=htmljinja<CR>

" --- JavaScript ---
let javascript_enable_domhtmlcss=1
autocmd BufNewFile,BufRead *.json setlocal ft=javascript
" переключение на синтаксис
nnoremap <leader>Tj :set ft=javascript<CR>

" --- CSS ---
" переключение на синтаксис
nnoremap <leader>Tc :set ft=css<CR>

">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ОТСТУПЫ
"Автоотступ
set autoindent

" --- Python ---
autocmd FileType python,pyrex setlocal expandtab shiftwidth=4 tabstop=8 formatoptions+=croq softtabstop=4 smartindent
			\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
" --- HTML, Templates ---
autocmd FileType html,xhtml,xml,htmldjango,htmljinja,eruby,mako setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2

" --- CSS ---
autocmd FileType css setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4

">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ПОДСВЕТКА
" включить подсветку кода
syntax on                    
" вкл/выкл Ctrl-C
function! ToggleSyntax()
if exists("g:syntax_on")
syntax off
else
syntax enable
endif
endfunction
nmap <silent> <C-c> :call ToggleSyntax()<CR>

" --- Python ---
" подстветка синтаксиса
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> АВТОДОПОЛНЕНИЕ
"  убрать раздражающие всплывающие окна с документацией для omnicompletion:
" set completeopt-=preview
" версия python3 для jedi-vim
let g:jedi#force_py_version = 3
" supertab для jedi
let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabMappingForward = '<tab>'
let g:SuperTabMappingBackward = '<s-tab>'

" --- Python ---
" отключаем автокомплит по коду (у нас вместо него используется jedi-vim)
let g:pymode_rope = 0
let g:pymode_rope_completion = 0
let g:pymode_rope_complete_on_dot = 0
" запретить автоматический выбор первого варианта
" let g:jedi#popup_select_first = 0

" --- CSS ---
" autocmd FileType css set omnifunc=csscomplete#CompleteCSS

" --- JavaScript ---
" autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS

" --- HTML ---
" autocmd FileType html set omnifunc=htmlcomplete#CompleteTags

" каталог с настройками SnipMate
let g:snippets_dir = "~/.vim/vim-snippets/snippets"

">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ОТЛАДКА
" --- Python ---
" возможность отладки pymode
let g:pymode_breakpoint = 1
" установка breakpoints [,b]
let g:pymode_breakpoint_key = '<leader>b'
">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ФОРМАТИРОВАНИЕ
" ,= 
autocmd FileType python nnoremap <LocalLeader>= :0,$!yapf<CR>

">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ПРОВЕРКА

" --- Python ---
" возможность проверки Pymode
let g:pymode_lint = 1
let g:pymode_lint_checker = "pyflakes,pep8"
let g:pymode_lint_ignore="E501,W601,C0110"
" провека кода после сохранения
let g:pymode_lint_on_write = 0  
" проверка кода в соответствии с PEP8 <,8>
autocmd FileType python map <buffer> <leader>8 :PymodeLint<CR>    

">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ДОКУМЕНТАЦИЯ

" --- Python ---
" jedi-vim показываeт документацию по методу/классу
" autocmd FileType python set completeopt-=preview 
" документация
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'

"=====================================================
"Клавиши быстрого редактирования строки в режиме вставки
"и в режиме редактирования командной строки.
"=====================================================
imap <C-H> <BS>
imap <C-J> <Left>
imap <C-K> <Right>
imap <C-L> <Del>
cmap <C-H> <BS>
cmap <C-J> <Left>
cmap <C-K> <Right>
cmap <C-L> <Del>
"=====================================================
" Автоматическое закрытие скобок
"=====================================================
imap [ []<LEFT>
inoremap ( ()<LEFT>
inoremap { {}<LEFT>
inoremap " ""<LEFT>
inoremap ' ''<LEFT>
inoremap <c-,> '',<LEFT><LEFT>
"(   ):"
inoremap <c-9> ():<LEFT><LEFT>
"Для CSS
imap <c-]> {<LEFT><return><esc><s-o> 
"=====================================================
" Тема
"=====================================================
" включаем 256 цветов 
set t_Co=256

" подсвечиваем все что можно подсвечивать
let python_highlight_all = 1

" сплошной разделитель окон
set fillchars+=vert:\  

" скрыть тильды
:highlight NonText ctermfg=234         

" цвет всплывающего меню автодополнения
:highlight Pmenu ctermbg=238

"=====================================================
set scrolloff=5	     " 5 строк при скролле за раз

" настройка на Tab
" set smarttab
" set tabstop=4

" TaskList настройки
map <leader>t :TaskList<CR> 	   " отобразить список тасков

aunmenu Help.
aunmenu Window.
let no_buffers_menu=1
set mousemodel=popup

set ruler
" set gcr=a:blinkon0
set ttyfast

"tab sball
set switchbuf=useopen

